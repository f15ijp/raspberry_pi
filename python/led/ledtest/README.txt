Sample from http://www.raspberrypi-spy.co.uk/2012/06/control-led-using-gpio-output-pin/

The following header pins are required :

* Header Pin 2 : 5V
* Header Pin 6 : Ground
* Header Pin 11 : GPIO


* The LED is a standard 5mm red type with a forward voltage of 2V.
* The transistor could be a BC547, BC548 or equivalent.
* Resistor R1 limits current through the LED from the 5V pin. If the 
  voltage drop across the LED is 2V then voltage across the resistor is 
  3V. So current is 3/560 = 5.4mA.
* Resistor R2 limits current from the GPIO pin. GPIO is either 0v or 
  3.3V so the maximum current into base of transistor is 
  3.3/27000 = 120uA.
* Assuming the gain of the transistor is 200 a base current of 120uA 
  would allow a maximum of 24mA (120uA x 200) to pass through the LED.

R1 could be increased you just need to make sure you allow enough 
current to power your LED.

R2 could be increased and this would reduce the current drawn from the 
GPIO pin but it would reduce the maximum current allowed to flow through
the transistor.

Note : As with all connections you make to the Pi�s PCB you must double
check everything. Making a mistake in your wiring could damage the CPU 
as some of the GPIO pins are connected straight to the Broadcom chip.

Python Code

In order to control GPIO pins in Python you must install the RPi.GPIO 
library. Installing the library is easy if you follow RPi.GPIO 
Installation Guide (
http://www.raspberrypi-spy.co.uk/2012/05/install-rpi-gpio-python-library/
) Once installed you can use the following Python script to toggle the 
GPIO output on header pin 11 (GPIO17)
